/**
 * @file common/Links.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef COMMON_LINKS
#define COMMON_LINKS

#include <artis-star/common/Node.hpp>
#include <artis-star/common/utils/String.hpp>

#include <map>
#include <sstream>

namespace artis::common {

template<typename Time>
class Node;

template<typename Time>
class Links : public std::multimap<Node<Time>, Node<Time> > {
  typedef std::multimap<Node<Time>, Node<Time>> type;

public:
  typedef std::pair<typename Links<Time>::const_iterator, typename Links<Time>::const_iterator> Result;

  Links() = default;

  virtual ~Links() = default;

  void
  add(Model<Time> *src_model, unsigned int src_port_index, const common::InOutType &src_in_out, Model<Time> *dst_model,
      unsigned int dst_port_index, const common::InOutType &dst_in_out) {
    type::insert(std::make_pair(Node<Time>(src_model, src_port_index, src_in_out),
                                Node<Time>(dst_model, dst_port_index, dst_in_out)));
  }

  bool exist(Model<Time> *src_model, unsigned int src_port_index, const common::InOutType &src_in_out,
             Model<Time> *dst_model, unsigned int dst_port_index, const common::InOutType &dst_in_out) const {
    auto it = type::equal_range(Node<Time>(src_model, src_port_index, src_in_out));
    typename Links<Time>::const_iterator it2 = it.first;
    bool found = false;

    while (not found and it2 != it.second) {
      found = it2->second == Node<Time>(dst_model, dst_port_index, dst_in_out);
      ++it2;
    }
    return found;
  }

  typename Links<Time>::const_iterator
  find(Model<Time> *src_model, unsigned int src_port_index, const common::InOutType &src_in_out,
       Model<Time> *dst_model, unsigned int dst_port_index, const common::InOutType &dst_in_out) const {
    auto it = type::equal_range(Node<Time>(src_model, src_port_index, src_in_out));
    typename Links<Time>::const_iterator it2 = it.first;
    bool found = false;

    while (not found and it2 != it.second) {
      found = it2->second == Node<Time>(dst_model, dst_port_index, dst_in_out);
      if (not found) {
        ++it2;
      }
    }
    return it2;
  }

  Links::Result find(Model<Time> *src_model, unsigned int src_port_index, const common::InOutType &src_in_out) const {
    return type::equal_range(common::Node<Time>(src_model, src_port_index, src_in_out));
  }

  void remove(common::Model<Time> *src_model, unsigned int src_port_index, const common::InOutType &src_in_out,
              common::Model<Time> *dst_model, unsigned int dst_port_index, const common::InOutType &dst_in_out) {
    typename Links<Time>::const_iterator result = find(src_model, src_port_index, src_in_out, dst_model, dst_port_index,
                                                       dst_in_out);

    this->erase(result);
  }

  void remove_links(common::Model<Time> *model) {
    typename Links<Time>::iterator it = this->begin();

    while (it != this->end()) {
      if (it->first.get_model() == model or it->second.get_model() == model) {
        this->erase(it);
        it = this->begin();
      } else {
        ++it;
      }
    }
  }

  std::string to_string(int level = 0) const {
    std::stringstream ss;

    ss << common::String::make_spaces(level * 2) << "links:" << std::endl;
    for (typename Links<Time>::const_iterator it = Links<Time>::begin(); it != Links<Time>::end(); ++it) {
      ss << common::String::make_spaces((level + 1) * 2)
         << it->first.get_model()->get_name() << "::"
         << (it->first.get_in_out() == common::InOutType::IN ? it->first.get_model()->get_in_port_name(it->first.get_port_index()) :
                                                              it->first.get_model()->get_out_port_name(it->first.get_port_index()))
         << " -> "
         << it->second.get_model()->get_name() << "::"
         << (it->second.get_in_out() == common::InOutType::IN ? it->second.get_model()->get_in_port_name(it->second.get_port_index()) :
                                                               it->second.get_model()->get_out_port_name(it->second.get_port_index()))
         << std::endl;
    }
    return ss.str();
  }
};

} // namespace artis common

#endif
