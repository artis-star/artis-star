/**
 * @file common/observer/Observer.hpp
 * @author The ARTIS Development Team
 * See the AUTHORS or Authors.txt file
 */

/*
 * ARTIS - the multimodeling and simulation environment
 * This file is a part of the ARTIS environment
 *
 * Copyright (C) 2013-2024 ULCO http://www.univ-littoral.fr
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef ARTIS_OBSERVER_OBSERVER_HPP
#define ARTIS_OBSERVER_OBSERVER_HPP

#include <artis-star/common/Model.hpp>
#include <artis-star/common/observer/View.hpp>

#include <vector>

namespace artis::common::observer {

template<typename Time>
class Observer {
public:
  typedef std::map<std::string, std::unique_ptr<View<Time>>> Views;

  Observer(const common::Model<Time> *model) : _model(model) {}

  Observer(const Observer<Time> &) = delete;

  Observer(const Observer<Time> &&) = delete;

  virtual Observer<Time> &operator=(const Observer<Time> &) = delete;

  virtual Observer<Time> &operator=(const Observer<Time> &&) = delete;

  virtual ~Observer() = default;

  void attachView(const std::string &name, View<Time> *view) {
    _views[name].reset(view);
    view->attachModel(_model);
  }

  void clear() {
    for (const auto &view: _views) {
      view.second->clear();
    }
  }

  const View<Time> &view(const std::string &name) const {
    return *_views.find(name)->second;
  }

  const Views &views() const { return _views; }

  void init() {
    for (const auto &view: _views) {
      view.second->init();
    }
  }

  void observe(double time, double next_t, bool finish) {
    if (_step == 0) {
      observe(time);
      _last_time = time;
    } else {
      if (not _init) {
        observe(time);
        _last_time = time;
        _init = true;
      }

      double next_last_time = std::floor((_last_time + _step + epsilon) / _step) * _step;

      if (time <= next_last_time and (next_t > next_last_time or finish)) {
        do {
          observe(next_last_time);
          _last_time = next_last_time;
          next_last_time = std::floor((_last_time + _step + epsilon) / _step) * _step;
        } while (time < next_last_time and next_t > next_last_time);
      }
    }
  }

  void switch_to_timed_observer(double step) {

    assert(step > 0);

    _step = step;
  }

private:
  void observe(double time) {
    for (const auto &view: _views) {
      view.second->observe(time, _step == 0);
    }
  }

  constexpr static double epsilon = 1e-6;

  double _step{0};
  const common::Model<Time> *_model;
  Views _views;
  double _last_time{0};
  bool _init{false};
};

}

#endif
